import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Header } from './components/Header'
import { Watchlist } from './components/Watchlist'
import { Add } from './components/Add'
import './App.scss'

import { GlobalProvider } from './context/GlobalState'

function App() {
	return (
		<GlobalProvider>
			<Router>
				<div className='background'>
					<Header />
					<Routes>
						<Route exact path='/' element={<Watchlist />} />
						<Route path='/add' element={<Add />} />
					</Routes>
				</div>
			</Router>
		</GlobalProvider>
	)
}

export default App
