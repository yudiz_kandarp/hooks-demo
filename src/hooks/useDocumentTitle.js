import { useEffect } from 'react'

function useDocumentTitle(value) {
	useEffect(() => {
		document.title = value
	}, [value])
}

export default useDocumentTitle
