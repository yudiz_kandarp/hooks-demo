import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'
import useDocumentTitle from '../hooks/useDocumentTitle'
import MovieCard from './MovieCard'

export const Watchlist = () => {
	const { watchlist } = useContext(GlobalContext)
	useDocumentTitle('watchlist')
	return (
		<div className='movie-page'>
			<div className='container'>
				<div className='header'>
					<h1 className='heading'>My Watchlist</h1>

					<span className='count-pill'>
						{watchlist.length} {watchlist.length === 1 ? 'Movie' : 'Movies'}
					</span>
				</div>

				{watchlist.length > 0 ? (
					<div className='movie-grid anim'>
						{watchlist.map((movie) => (
							<MovieCard movie={movie} key={movie.id} />
						))}
					</div>
				) : (
					<h2 className='no-movies'>No movies in your list! Add some!</h2>
				)}
			</div>
		</div>
	)
}
