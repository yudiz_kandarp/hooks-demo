import React from 'react'
import PropTypes from 'prop-types'
import '../styles/Button.scss'
function Button({ primary, children }) {
	return (
		<div className={`button ${primary ? 'primary' : ''}`} disabled>
			{children}
		</div>
	)
}
Button.propTypes = {
	primary: PropTypes.bool,
	children: PropTypes.string,
}

export default Button
