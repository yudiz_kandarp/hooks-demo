/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import useDocumentTitle from '../hooks/useDocumentTitle'
import '../styles/add.scss'
import { ResultCard } from './ResultCard'

export const Add = () => {
	const [query, setQuery] = useState('')
	const [results, setResults] = useState([])
	const [loading, setLoading] = useState(false)
	const [message, setMessage] = useState(false)
	const inputRef = useRef(null)

	useDocumentTitle('add to watchlist')
	useEffect(function setFocusOnLoad() {
		inputRef.current.focus()
		
	}, [])

	useEffect(
		function filterData() {
			if (query?.length == 0) return
			const TimeOut = setTimeout(function fetchFilteredData() {
				delayedSearch()
			}, 2000)
			return () => clearTimeout(TimeOut)
		},
		[query]
	)

	async function delayedSearch() {
		console.log('fetching')
		setLoading(true)
		setMessage(false)
		try {
			const response = await fetch(
				`https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1&include_adult=false&query=${query}`
			)
			const data = await response.json()
			if (!data.errors) {
				setResults(data.results)
				if (!data.results.length) setMessage(true)
				setLoading(false)
			} else {
				setResults([])
				setLoading(false)
			}
		} catch (error) {
			console.log('not')
		}
	}

	const onChange = (e) => {
		e.preventDefault()
		if (e.target.value.trim() != '') {
			setLoading(true)
			setQuery(e.target.value)
		} else {
			setLoading(false)
			setQuery(e.target.value)
			setResults([])
		}
	}

	return (
		<div className='add-page'>
			<div className='container'>
				<div className='add-content'>
					<div className='input-wrapper'>
						<input
							type='text'
							placeholder='Search for a movie'
							ref={inputRef}
							value={query}
							onChange={onChange}
						/>
						{loading && <div className='loading' />}
					</div>

					{message && <div className='no-movies'> No Results Found</div>}
					{results?.length > 0 && (
						<ul className='results'>
							{results.map((movie) => (
								<li key={movie.id} className={loading ? '' : 'anim'}>
									<ResultCard movie={movie} />
								</li>
							))}
						</ul>
					)}
				</div>
			</div>
		</div>
	)
}
