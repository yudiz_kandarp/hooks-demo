import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/header.scss'
import Button from './Button'

export const Header = () => {
	return (
		<header>
			<div className='container'>
				<div className='inner-content'>
					<div className='brand'>
						<Link className='a' to='/'>
							WatchList
						</Link>
					</div>

					<Link to='/add'>
						<Button>+ Add</Button>
					</Link>
				</div>
			</div>
		</header>
	)
}
