/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'

export const MovieControls = ({ movie }) => {
	const { removeMovieFromWatchlist } = useContext(GlobalContext)

	return (
		<div className='inner-card-controls'>
			<div>
				<button
					className='ctrl-btn'
					onClick={() => removeMovieFromWatchlist(movie.id)}
				>
					Remove
				</button>
			</div>
		</div>
	)
}
