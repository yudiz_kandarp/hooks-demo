import React from 'react'
import PropTypes from 'prop-types'
import { MovieControls } from './MovieControls'

function MovieCard({ movie }) {
	return (
		<div className='movie-card'>
			<div className='overlay'></div>
			{movie.poster_path != null ? (
				<img
					src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`}
					alt={`${movie.title} Poster`}
				/>
			) : (
				<>
					<div className='filler-poster-watchlist'>
						image not found
						<div>{movie.title}</div>
					</div>
				</>
			)}

			<MovieControls movie={movie} />
		</div>
	)
}

MovieCard.propTypes = {
	movie: PropTypes.shape({
		title: PropTypes.string,
		poster_path: PropTypes.string,
	}),
}

export default MovieCard
